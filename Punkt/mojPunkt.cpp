#include<iostream>
#include"projekt2D.h"
using namespace std;
int main() {
	int test_int;
	float test_float;		// zdefiniowa�am dwa typy dla mojego punktu
	Point moj_punkt;

	cout << "Rozmiar int" << sizeof(test_int) << endl;
	cout << "Rozmiar float" << sizeof(test_float) << endl;
	cout << "Rozmiar point" << sizeof(moj_punkt) << endl;

	moj_punkt.setX(5.777);

	cout << "Wspolrz�dna X mojego obiektu " << moj_punkt.getX() <<endl;
	cout << "Wspolrz�dna Y mojego obiektu " << moj_punkt.getY() << endl;
	cout << "-----------------\n";
	cout << "Podaj wspolrz�dn� X:\n";
	float tmpX;
	cin >> tmpX;
	cout << "Podaj wspolrz�dn� Y:\n";
	float tmpY;
	cin >> tmpY;
	
	Point new_point;
	new_point.setX(tmpX);
	new_point.setY(tmpY);
	cout << "Wspolrz�dna X new_obiektu " << new_point.getX() << endl;
	cout << "Wspolrz�dna Y new_obiektu " << new_point.getY() << endl;
	cout << "-----------------\n";
	Point test_point(tmpX, tmpY);
	cout << "Wspolrz�dna X test_obiektu " << test_point.getX() << endl;
	cout << "Wspolrz�dna Y test_obiektu " << test_point.getY() << endl;
	cout << "-----------------\n";
	Point jeden_point(tmpX);
	cout << "Wspolrz�dna X jeden_obiektu " << jeden_point.getX() << endl;
	cout << "Wspolrz�dna y jeden_obiektu " << jeden_point.getY() << endl;
	cout << "-----------------\n";
	

	cout << "Podaj a: \n";
	float a;
	cin >> a;
	cout << "Podaj b: \n";
	float b;
	cin >> b;
	cout << "Podaj numer �wiartki: \n";
	int c;
	cin >> c;
	Point quarter_point(a, b, c);
	cout << "Wspolrz�dna X jeden_obiektu " << quarter_point.getX() << endl;
	cout << "Wspolrz�dna y jeden_obiektu " << quarter_point.getY() << endl;
	
	system("pause");
	return 0;
}
