#pragma once
#include<iostream>

#include"projekt2D.h"

Point::Point() {
	std::cout << "Dzia�a konstruktor klasy Punkt bez argument�w.\n";
	x = 0.0;
	y = 0.0;
}
Point::Point(float wspX, float wspY) : x(wspX), y(wspY){
	std::cout << "Dzia�a konstruktor klasy Punkt z argumentami.\n";
}
Point::Point (float a, float b, int c){
	if (c<1 || c>4) {
		std::cout << "Nieprawid�owe dane: niepoprawny numer �wiartki.\n";
		std::cout << " Obiekt zosta� zainicjalizowany domy�lnymi warto�ciami.\n";
		x = 0.0;
		y = 0.0;
	}
	else if (a>=0.0 || b>=0.0 && c==1){
		std::cout<<"Dane poprawne.\n";
		x = a;
		y = b;
	}
	else if (a<0.0 || b>0.0 && c==2){
		std::cout << "Dane poprawne. �wiartka druga.\n";

	}
	else if (a<0.0 || b<0.0 && c == 3) {
		std::cout << "Dane poprawne, �wiartka trzecia.\n";

	}
	else if (a>0.0 || b<0.0 && c == 4) {
		std::cout << "Dane poprawne, �wiartka czwarta.\n";

	}
	else {
		std::cout << "Wprowadzono b��dne dane.\n";
		std::cout << " Obiekt zosta� zainicjalizowany domy�lnymi warto�ciami.\n";
		x = 0.0;
		y = 0.0;
	}
}
Point::Point(float aaa): x(aaa),y(aaa) {
	std::cout << "Konstruktor z jednym argumentem. Czy dzia�a ???\n";
}

// podaj�, z jakiej klasy pochodzi funkcja
void Point::setX(float a) {x = a;}

void Point::setY(float b) {y = b;}

float Point::getX(){ return x; }		// zdefiniowali�my prototyp/struktur� klasy
float Point::getY() { return y; }