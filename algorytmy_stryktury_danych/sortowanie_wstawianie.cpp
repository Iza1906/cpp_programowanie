#include <iostream>
using namespace std;
int main() {
	//utw�rz tablic� z liczbami nieposortowanymi np. liczby ca�kowite
	// za�o�enie: tablica statyczna 10-elementowa
	int tablica[10] = { 9,3,0,7,1,15,100,6,-5,2 };
	// wy�wietl t� tablic� (nieposortowan�)
	int n = 10;						//wielko�c tablicy
	for (int i=0; i<n;i++) {			//n-1 bo numeracja zaczyna si� od 0
		cout << tablica[i] << ", ";
	}
	cout << endl;
	// posortuj tablic� (wyk�ad 1 str. 21)
	for (int i=1;i<n;i++) {
		for (int j = i;j >= 1;j--) {
			if (tablica[j] < tablica[j - 1]) {
				//zamie� tablica[j] z tablica[i]
				int tymczasowa=tablica[j]; 
				tablica[j] = tablica[j - 1]; 
				tablica[j - 1] = tymczasowa;
			}
		 }
	}
	//wy�wietl tablic� w celu sprawdzenia czy sortowanie si� uda�o
	
	for (int i = 0; i<n;i++) {			//n-1 bo numeracja zaczyna si� od 0
		cout << tablica[i] << ", "<<endl;
	}
	system("PAUSE");
}