#include <iostream>				// biblioteka, w kt�rej jest cin; strumie� wej�cia-wyj�cia
using namespace std;			// przestrze� nazw
double silnia(int);
int main() {
	int N;						//zadeklaruj n z klawiatury
	cout << "Program liczy silnie podanej liczby naturalnej n.";
	cout << endl;
	cout << "Podaj n= ";
	cin >> N;								//wczytaj n z klawiatury
	// sprawdz, czy n>=0
	if (N >= 0) {
	// je�eli tak, to policz n!
	// wywo�anie funkcji silnia
		double wynik;
		wynik = silnia(N);
		// wypisz wynik
		cout << "Wynik= " << wynik << endl;
	}
	else {
		cout << "Liczba nie mo�e by� ujemna !" << endl;// je�eli nie, to zako�cz program z komunikatem b��du
	}
	system("PAUSE");

}

double silnia(int N) {			// funkcja przyjmie argument typu int, ale zwr�ci wynik typu double

	// sprawd�, czy N=0 ?
	double wynik = 1;
	if (N == 0) {
		//je�li tak, to wynik =1;
		wynik = 1;
	}
	else {
		//je�eli nie, to wynik =1*2*3....*n
		// p�tla od i=1 do n, kt�ra policzy silni�
		wynik = 1;
		for (int i = 1; i <= N; i++) {
			wynik = wynik*i;
		}

	}
	return wynik;
}