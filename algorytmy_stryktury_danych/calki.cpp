#include<iostream>
using namespace std;
double integrate_monte_carlo(double a, double b, int n);
double f(double x);
// ca�kowanie numeryczne
// program g��wny: 
// wczytaj granice ca�kowania a i b
// wczytaj liczb� losowa� (prostok�t�w: n)
// wywo�aj funkcj� ca�kuj�c�
// wypisz wynik
// zako�cz
int main() {
	double a;
	double b;
	int n;
	cout << "Program liczy ca�k� z funkcji x^2 w przedziale od a do b" << endl;
	cout << "Podaj a= " << endl;
	cin >> a;
	cout << "Podaj b= "<<endl;
	cin >> b;
	cout << "Podaj liczb� losowa�"<<endl;
	cin >> n;
	cout << "Ca�ka wynosi: " << integrate_monte_carlo(a, b, n) << endl;
	system("pause");
}
double integrate_monte_carlo(double a, double b, int n) {
	double suma = 0;
	for (int i = 0;i<n;i++) {
		double x_random_a_b =(double)rand()*((b-a)+a)/(double)RAND_MAX;
			suma = suma + f(x_random_a_b);
	}
	double wynik =(b - a)/n*suma;
		return wynik;
}
double f(double x) {
	return x*x;

}