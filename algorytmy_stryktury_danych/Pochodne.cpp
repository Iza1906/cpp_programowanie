#include<iostream>
using namespace std;
double f(double x);
double fp(double x, double h);
double fpp(double x, double h);
// Numeryczne obliczanie pochodnych
// Program g��wny:
// wczytaj x,h
// wylicz i wypisz:
//- warto�� pierwszej pochodnej funkcji f w punkcie x
//- warto�� drugiej pochodnej funkcji f w punkcie x
// zako�cz

void main()
{
	double x;
	double h;
	cout << "Podaj warto�� x: " << endl;
	cin >> x;
	cout << "Podaj warto�� h: " << endl;
	cin >> h;
	cout << "Pierwsza pochodna wynosi: " << fp(x, h) << endl;
	cout << "Druga pochodna wynosi: " << fpp(x, h)<< endl;
	system("Pause");
}
// funkcja f(x)= x^2 (x do kwadratu)
double f(double x) {
	return x*x;
}
// funkcja fp(x)=2x wz�r na pierwsz� pochodn�
double fp(double x, double h) {
	double wynik;
	wynik = (f(x + h)-f(x - h)) / (2 * h);
	return	wynik;			//we wzorze wyst�puje h, wi�c nale�y go jako� przekaza� do funkcji
}
// funkcja fpp(x)=2 wz�r na drug� pochodn�
double fpp(double x, double h) {
	double wynik;
	wynik = (f(x + h) + f(x - h)-2*f(x))/(h*h);
	return wynik;
}
