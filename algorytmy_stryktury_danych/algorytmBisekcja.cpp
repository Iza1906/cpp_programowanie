#include <iostream>
using namespace std;
double bisection(double a, double b, double eps);	// prototypy funkcji
double f(double x);
// program glowny ma wczytac a,b,eps
// sprawdz, czy w [a,b] jest miejsce zerowe funkcji f(x)
// jezeli tak, to wywo�aj funkcje bisekcja(...) i wyswietl wynik na ekranie
// jezeli nie to wypisz komunikat "Bledny przedzial" i zakoncz program
// musimy zdefiniowac funkcje bisection(...), ktora policzy miesca zerowe i je zwroci do programu glownego
// musimy tez miec prosta funkcje f(x)=x-5, ktorej miejsca zerowe znamy

int main() {
	double a, b, eps;
	cout << "Funkcja znajduje miejsce zerowe zadanej funkcji w przedziale [a,b] z dok�adnoscia epsilon" << endl;
	cout << " Podaj a= ";
	cin >> a;
	cout << " Podaj b= ";
	cin >> b;
	// je�eli b>a to zamien
	if (a > b) {
		double tmp = a;
		a = b;
		b = tmp;
	}
	cout << " Podaj eps= ";
	cin >> eps;
		if (f(a)*f(b) < 0) {
		// licz i wy�wietl wynik
		double rezultat = bisection(a, b, eps);
		cout << "Miejsce zerowe znalezione dla x= " << rezultat << endl;
		}
		else {
		// nie licz i wyswietl odpowiedni komunikat
		cout << "W podanym przedziale nie ma miejsca zerowego" << endl;
		}
	system("pause");
	return 0;
}
double bisection(double a, double b, double eps) {		//definicja funkcji bisekcja
	double s;
	do {
		// wylicz �rodek przedzia�u
		s = (a + b) / 2;
		// je�eli f(x)==0 to ko�cz
		if (f(s) == 0) {
			return s;
		}
		if (f(a)*f(s) < 0) {
			// pierwiastek jest [od a do �rodek]
			b = s;
		}
		else {
			// pierwiastek jest [od �rodek do b]
			a = s;
		}
	}
		while (b - a > eps);
		return s;
	}
double f(double x) {	//definicja funkcji f
return x - 5;		// mo�emy tez zdefiniowac nowa zmienna  double wartosc; wartosc=x-5; return wartosc;
}
