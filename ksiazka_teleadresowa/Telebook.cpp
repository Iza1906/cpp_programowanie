#pragma once
#include "Telebook.h"
using namespace std;

TeleBook::TeleBook()
{
	cout << "Dzia�a konstruktor bez argument�w.\n";
}



TeleBook::TeleBook(Person imie_nazwisko_osoby, Date data_urodzenia_osoby)\
	:osoba(imie_nazwisko_osoby), data_urodzenia(data_urodzenia_osoby)
{
	cout << "Dzia�a konstruktor klasy TeleBook z argumentami.\n";
}

void TeleBook::setOsoba(Person os)
{osoba = os;}

void TeleBook::setData_ur(Date d_ur)
{
	data_urodzenia = d_ur;			// do obiektu data_urodzenia przypisuj� zmienn�
}

Person TeleBook::getOsoba()
{
	return osoba;
}

Date TeleBook::getData_ur()
{
	return data_urodzenia;
}
