#pragma once

#include<iostream>
#include "Person.h"
#pragma once
using namespace std;
Person::Person()
{
	cout << "Dzia�a konstruktor klasy Person bez argument�w.\n";
	imie ="Imie";
	nazwisko ="Nazwisko";
}
Person::Person(string imie_osoby, string nazwisko_osoby)\
	:imie(imie_osoby), nazwisko(nazwisko_osoby)
{
	cout << "Dzia�a konstruktor klasy Person z argumentami\n";
}

void Person::setImie(string im){imie = im;}

void Person::setNazwisko(string nazw){nazwisko = nazw;}

string Person::getImie(){return imie;}

string Person::getNazwisko(){return nazwisko;}
