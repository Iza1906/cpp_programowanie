#pragma once					// zabezpiecza przed kompilacj� plik�w, kt�re zosta�y ju� raz skompilowane
#include"Person.h"			// nie zawsze trzeba wrzuca� wszystkie klasy
#include "date.h"
class TeleBook
{
	Person osoba;
	Date data_urodzenia;
	public:
	// tworzymy konstruktora
		TeleBook();
		// tworzymy konstruktora z argumentami
		TeleBook(Person imie_nazwisko_osoby, Date data_urodzenia_osoby);

		void setOsoba(Person);
		void setData_ur(Date);

		Person getOsoba();			//osoba zwraca typ Person
		Date getData_ur();
};