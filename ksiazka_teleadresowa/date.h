#pragma once

class Date 
{
	int dzien;
	int miesiac;
	int rok;
public:
	Date();

	Date(int dzien_urodzenia, int m_urodzenia, int r_urodzenia);// konstruktor

		void setDzien(int);
		void setMiesiac(int);
		void setRok(int);

		int getDzien();
		int getMiesiac();
		int getRok();

};
