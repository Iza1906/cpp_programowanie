#pragma once

#include<iostream>
#include "date.h"

using namespace std;

Date::Date() : dzien(1), miesiac(1), rok(1900)
{
	cout << "Dzia�a konstruktor klasy Date bez argument�w.\n";
}
Date::Date(int dz, int m, int r)
{
	cout << "Dzia�a konstruktor klasy Date z argumentami.\n";
	dzien = dz;
	miesiac = m;
	rok = r;
}
void Date::setDzien(int dz) { dzien = dz; }
void Date::setMiesiac(int m) { miesiac = m; }
void Date::setRok(int r) { rok = r; }


int Date::getDzien(){return dzien;}

int Date::getMiesiac() { return miesiac; }

int Date::getRok(){return rok;}
