#pragma once
#include<iostream>
using namespace std;

class Person {
	string imie;
	string nazwisko;
public:
	Person();
	Person(string imie_osoby, string nazwisko_osoby);		// konstruktor
	void setImie(string);			//daje dost�p do zmiennych prywatnych
	void setNazwisko(string);

	string getImie();				// funkcje zwracaj�ce stringi
	string getNazwisko();

};
